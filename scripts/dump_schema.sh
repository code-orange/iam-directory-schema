#!/bin/bash

ldapsearch -LLL -o ldif-wrap=no -x -H "ldap://${ldap_ip}" -D "${ldap_bind_dn}" -w "${ldap_bind_pw}" -b "CN=Aggregate,CN=Schema,CN=Configuration,${ldap_dn_dc}" objectclass=subschema attributeTypes dITStructureRules objectClasses nameForms dITContentRules matchingRules ldapSyntaxes matchingRuleUse | sed '1d' > code-orange-iam-openldap.schema

ldapsearch -LLL -o ldif-wrap=no -x -H "ldap://${ldap_ip}" -D "${ldap_bind_dn}" -w "${ldap_bind_pw}" -b "CN=Schema,CN=Configuration,${ldap_dn_dc}" -E pr=1000/noprompt '(|(objectClass=attributeSchema)(objectClass=classSchema))' | grep -v '^\repsTo::' | grep -v '^\repsFrom::' | grep -v '^\replUpToDateVector::' | grep -v '^\# pagedresults:' > code-orange-iam-ad.ldif

#sed -i 's/1\.2\.840\.113556\.1\.4\.905/2\.5\.5\.4/g' code-orange-iam-openldap.schema
#sed -i 's/1\.2\.840\.113556\.1\.4\.906/2\.5\.5\.16/g' code-orange-iam-openldap.schema
#sed -i 's/1\.2\.840\.113556\.1\.4\.907/2\.5\.5\.15/g' code-orange-iam-openldap.schema

git add code-orange-iam-openldap.schema || true
git commit -m "update code-orange-iam-openldap.schema" || true

git add code-orange-iam-ad.ldif || true
git commit -m "update code-orange-iam-ad.ldif" || true
